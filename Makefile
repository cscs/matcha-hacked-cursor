CWD := $(shell pwd)

.PHONY: all
all: clean build

.PHONY: install
install: build
	@echo ::: INSTALL :::
	@sudo mkdir -p /usr/share/icons
	@sudo cp -r Matcha-Hacked /usr/share/icons/Matcha-Hacked

.PHONY: build
build:Matcha-Hacked

.PHONY: clean
	-@rm -rf build Matcha-Hacked &>/dev/null | true
	@echo ::: CLEAN :::

.PHONY: uninstall
uninstall:
	@echo ::: UNINSTALL :::
	@sudo rm -rf /usr/share/icons/Matcha-Hacked

Matcha-Hacked:
	@echo ::: BUILD :::
	@./build.sh > /dev/null 2>&1