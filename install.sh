#!/bin/sh

set -e

gh_repo="matcha-hacked-cursor"
gh_desc="Matcha Hacked Cursor"

cat <<- EOF


M    M          t         h                  H   H               k                         CC                                 
MM  MM   aaa    t    cc   h       aaa        H   H   aaa    cc   k  k   eee       d       C  C  u  u   rr   ssss   ooo    rr  
M MM M      a  ttt  c  c  hhhh       a       HHHHH      a  c  c  k k   e   e      d       C     u  u  r    s      o   o  r    
M    M   aaaa   t   c     h   h   aaaa  ---  H   H   aaaa  c     kk    eeeee   dddd  ---  C     u  u  r     sss   o   o  r    
M    M  a   a   t   c  c  h   h  a   a       H   H  a   a  c  c  k k   e      d   d       C  C  u  u  r        s  o   o  r    
M    M   aaaa   tt   cc   h   h   aaaa       H   H   aaaa   cc   k  k   eee    dddd        CC    uuu  r    ssss    ooo   r                                


  $gh_desc
  https://gitlab.com/cscs/$gh_repo


EOF

: "${PREFIX:=/usr/share/icons}"
: "${TAG:=master}"
: "${uninstall:=false}"

_msg() {
    echo "=>" "$@" >&2
}

_rm() {
    # removes parent directories if empty
    sudo rm -rf "$1"
    sudo rmdir -p "$(dirname "$1")" 2>/dev/null || true
}

_download() {
    _msg "Getting the latest version from GitLab ..."
    wget -O "$temp_file" \
        "https://gitlab.com/cscs/$gh_repo/-/archive/master/matcha-hacked-cursor-master.tar.gz"
    _msg "Unpacking matcha archive ..."
    tar -xzf "$temp_file" -C "$temp_dir"
}

_uninstall() {
    _msg "Deleting $gh_desc ..."
	sudo rm -rf "$PREFIX/Matcha-Hacked"
}

_build() {
    _msg "Building ..."
    chmod +x "$temp_dir/$gh_repo-$TAG/build.sh"
    bash "$temp_dir/$gh_repo-$TAG/build.sh" > /dev/null 2>&1
}

_install() {
    _msg "Installing ..."
    sudo cp -R \
        "$temp_dir/$gh_repo-$TAG/Matcha-Hacked" \
        "$PREFIX/"
}

_cleanup() {
    _msg "Clearing cache ..."
    rm -rf "$temp_file" "$temp_dir" \
        ~/.cache/plasma-svgelements-Matcha* \
        ~/.cache/plasma_theme_Matcha*.kcache
    _msg "Done!"
}

trap _cleanup EXIT HUP INT TERM

temp_file="$(mktemp -u)"
temp_dir="$(mktemp -d)"

if [ "$uninstall" = "false" ]; then
    _download
    _uninstall
    _build
    _install
else
    _uninstall
fi
