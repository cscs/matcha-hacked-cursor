# Matcha-Hacked Cursor Theme

<p align="center">
  <img src="https://gitlab.com/cscs/matcha-hacked-cursor/raw/936f9292e47ca7902a8be77e02de4cf15e72357a/src/cursors.svg" alt="matcha-hacked-cursor-theme"/>
</p>

## Features

* Multiple sizes; 24px, 36px, and 48px
* Compatible with most Linux window managers
* Animated cursors

## DEPENDENCIES

- `bash` <br/>
- `cmake` <br/>
- `inkscape` <br/>
- `libcanberra` <br/>
- `xorg-xcursorgen` <br/>

## Installation

### Matcha Hacked Cursor Installer

Use the scripts to install the latest version directly from this repo (independently on your distro):

#### Install

```shell
curl -O https://gitlab.com/cscs/matcha-hacked-cursor/raw/master/install.sh
chmod +x install.sh
./install.sh
```

#### Uninstall

```shell
curl -O https://gitlab.com/cscs/matcha-hacked-cursor/raw/master/install.sh
chmod +x install.sh
env uninstall=true ./install.sh
```

### Donate  

Everything is free, but you can donate using these:  

<a href='https://ko-fi.com/X8X0VXZU' target='_blank'><img height='36' style='border:0px;height:36px;' src='https://az743702.vo.msecnd.net/cdn/kofi4.png?v=2' border='0' alt='Buy Me a Coffee at ko-fi.com' /></a> &nbsp; <a href='https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=M2AWM9FUFTD52'><img height='36' style='border:0px;height:36px;' src='https://gitlab.com/cscs/resources/raw/master/paypalkofi.png' border='0' alt='Donate with Paypal' />  

## License

[GPL 2.0 License](https://gitlab.com/cscs/matcha-hacked-cursor/raw/master/LICENSE)

## Credits

* Ken Vermette <vermette@gmail.com> - Cursor Author
* Hugo Pereira Da Costa <hugo.pereira@free.fr> - Kstyle Developer
* Andrew Lake <jamboarder@gmail.com> - Kstyle Designer
* Uri Herrera <kaisergreymon99@gmail.com> - Breeze Icon Theme
* [Jam Risser](https://jam.jamrizzi.com) <jam@jamrizzi.com> - Contributor
* FadeMind - For being the bees knees
